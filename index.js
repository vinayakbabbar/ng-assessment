// https://forms.maakeetoo.com/formsdata/857

// ======================Adding Range selector
const userCount = document.querySelector("#usersCount");
const userRange = document.querySelector("#formControlRange");
const card1 = document.querySelector(".card1");
const card2 = document.querySelector(".card2");
const card3 = document.querySelector(".card3");

setValue = () => {
  userCount.innerHTML = userRange.value;

  if (userRange.value <= 10) {
    card2.classList.remove("border");
    card3.classList.remove("border");
    card1.classList.add("border");
  } else if (userRange.value > 10 && userRange.value <= 20) {
    card1.classList.remove("border");
    card3.classList.remove("border");
    card2.classList.add("border");
  } else if (userRange.value > 20) {
    card1.classList.remove("border");
    card2.classList.remove("border");
    card3.classList.add("border");
  }
};

userRange.addEventListener("input", setValue);

// ============================lazy loading section

// API Website:-  https://www.slingacademy.com/article/sample-photos-free-fake-rest-api-for-practice/

const photoSectionDiv = document.getElementById("photo_section");

const loader = document.getElementById("loader");

let photosCount = 0;

let loading = false;

function fetchImages() {
  if (loading) return;

  loading = true;

  loader.style.display = "block";

  fetch(
    `https://api.slingacademy.com/v1/sample-data/photos?offset=${photosCount}&limit=5`
  )
    .then((response) => response.json())
    .then((data) => {
      loader.style.display = "none";

      data.photos.forEach((item) => {
        const PhotosImageTag = document.createElement("img");

        PhotosImageTag.classList.add("result-item");

        PhotosImageTag.srcset = item.url;
        PhotosImageTag.alt=`photos`

        photoSectionDiv.appendChild(PhotosImageTag);
      });

      photosCount = photosCount + 5;

      loading = false;
    })
    .catch((error) => {
      console.error("Error fetching data:", error);

      loading = false;
    });
}

function checkScrolling() {
  const scrollTop =document.documentElement.scrollTop || document.body.scrollTop;    

  const windowHeight = window.innerHeight;

  const documentHeight = document.documentElement.offsetHeight;

  if (scrollTop + windowHeight >= documentHeight - 100) {
    fetchImages();
  }
}

window.addEventListener("scroll", checkScrolling);

fetchImages();


