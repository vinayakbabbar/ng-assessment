
## Bootstrap & Lazy Loading Example



## Description

**Section A : Making a simple website with vanilla JS**

**Task 1 - BootStrap**

a. Use this [page](https://getbootstrap.com/docs/4.0/examples/pricing/), and created a modal - which gets called - whenever the user clicks on the pricing. The modal  contain a form - asking following details:
- [ ]  Name
- [ ]  Email
- [ ]  Order Comments
- From the bootstrap template, one will be able to click on any of the buttons of the three pricing tables, and fill in a form.

- The contents of form submission - is populated there. Sign up on https://forms.maakeetoo.com . Which is shown in video.

b. Use a slider - which should allow user to scroll between the number of users - and suppose if the number of users are 0-10, then first plan will be highlighted, 10-20, the second plan will be highlighted, and so on.

**Task 2 - vanilla JS**

The site core web vitals report of both desktop and mobile standards are full fill which is taken using lighthouse in developer tool and screenshots are available in the video.
https://clipchamp.com/watch/imSeTg7BMYi

***
**Section B: Lazy Loading To Avoid Pagination (vanilla js)**

To avoid pagination, Frontend devs came up with the solution of lazy loading, so when a user reaches the end of the page, more results are automatically loaded to have a good user experience.

1. I use Public API https://www.slingacademy.com/article/sample-photos-free-fake-rest-api-for-practice/

2. Avoid using any libraries.

3. I can automate the deployment

***

**Hosted Link** :-https://bootstrap-lazy-loading.netlify.app/

**Video Link** :-https://clipchamp.com/watch/imSeTg7BMYi
